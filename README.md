 document.getElementsByTagName("body").addEventListener("click", logThis);
 // error: SCRIPT438: Object doesn't support property or method 'addEventListener'
 
 However
 
 document.body.addEventListener("click", function(e){console.log(this + " " + e);}, false);
 //  [object HTMLBodyElement] [object PointerEvent]
 
 https://stackoverflow.com/questions/7167969/document-body-addeventlistener-is-not-working
 While this isn't a direct answer to your question, I hope that it is helpful nevertheless. Using an extension for a screen reader is unnecessary, in particular you might not want to update that extension regularly as the browser's user interface changes. Firefox supports a number of accessibility APIs which you can use directly from your application. These APIs have the advantage of being stable, once your implementation is done it should continue working (as far as I remember there was only one occasion where Mozilla broke compatibility with existing screen readers).
As to the actual question: you didn't tell anything about the context in which this code is executing. Normally, extensions run code in their XUL overlay meaning that the context is the browser window, not the web page. Which would explain why your code isn't working (it's a XUL document, no document.body there). What you probably mean is attaching an event listener to the <tabbrowser> element where the web pages are loaded: gBrowser.addEventListener(...). Side-note: If you really wanted to catch all events on a XUL document you should register an event listener on document or document.documentElement.
Btw, you can see error messages related to extensions in the Error Console (the article I link to explains how to enable it in the current Firefox versions).

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact